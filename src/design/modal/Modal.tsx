import { AnimatePresence } from 'framer-motion'
import { ModalContext, ModalCtx } from './ModalContext'
import { Portal, PortalProps } from '../Portal'

export interface ModalProps extends ModalCtx {
  children?: Children
  portalProps?: PortalProps
}

/**
 *  Modal mounts it's content onto a `react.Portal` appended to the body.
 *  By default the focus is locked inside of the modal content, as well as
 *  scroll is disabled for the rest of the document.
 *
 *  NOTE: For the focus lock to work properly, modal should contain at least
 *        one focusable element, otherwise, focus will be lost.
 */
export function Modal({ children, portalProps, ...rest }: ModalProps) {
  return (
    <ModalContext.Provider value={rest}>
      <ModalContext.Consumer>
        {({ isOpen, onExitComplete }) => (
          <AnimatePresence onExitComplete={onExitComplete}>
            {isOpen && <Portal {...portalProps}>{children}</Portal>}
          </AnimatePresence>
        )}
      </ModalContext.Consumer>
    </ModalContext.Provider>
  )
}
