import { useState } from 'react'
import { SpreadInput } from '@design/SpreadInput'

export function DemoSpreadInput() {
  const [code, setCode] = useState<string>('')

  return (
    <>
      <SpreadInput
        value={code}
        onChange={setCode}
        onComplete={code => console.log(`Code: ${code}`)}
        numChars={6}
      />

      <pre>{JSON.stringify(code, null, 2)}</pre>
    </>
  )
}
