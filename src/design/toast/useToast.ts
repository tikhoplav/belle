import { useCallback, useContext, useId } from 'react'
import { ToastContext } from './ToastContext'
import { Toast } from './types'

/**
 *  Allows to dispatch an app notification with toast.
 */
export function useToast() {
  const fallbackId = useId()
  const { add, update, remove } = useContext(ToastContext)

  return useCallback(
    (props: Partial<Toast>) => {
      const id = props.id || fallbackId

      add({ ...props, id })

      return {
        update: (props: Omit<Partial<Toast>, 'id'>) => update({ ...props, id }),
        remove: () => remove(id),
      }
    },
    [fallbackId, add, update, remove],
  )
}
