import { useCallback, useEffect, useRef, useState } from 'react'
import { styled } from 'styled-components'

export function SpreadInput({
  value,
  onChange,
  onComplete,
  numChars,
  className,
}: {
  value?: string
  onChange: (value: string) => void
  onComplete?: (value: string) => void
  numChars: number
  className?: string
}) {
  const ref = useRef<HTMLDivElement | null>(null)
  const [_value, setValue] = useState<string>(value || '')

  useEffect(() => {
    onChange(_value)
    if (_value.length == numChars) onComplete?.(_value)
  }, [_value, onChange, onComplete, numChars])

  const onInput = useCallback(
    (i: number, chars: string) => {
      const children = ref.current?.children
      if (!children) return
      const N = children.length

      setValue(cur => {
        const long = cur.slice(0, i) + chars + cur.slice(i + 1)
        return long.slice(0, N)
      })

      if (!chars) {
        return (children[i - 1] as HTMLInputElement)?.focus()
      }

      if (chars.length + i < N) {
        return (children[i + chars.length] as HTMLInputElement).focus()
      }
    },
    [setValue],
  )

  return (
    <Root
      className={className}
      ref={ref}
    >
      {Array.from({ length: numChars }, (_, i) => (
        <Input
          key={i}
          value={_value[i] || ''}
          onChange={e => onInput(i, e.target.value)}
          onFocus={e =>
            setTimeout(() => {
              e.target.selectionStart = 1
              e.target.selectionEnd = 1
            }, 0)
          }
        />
      ))}
    </Root>
  )
}

const Root = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 12px;
`

const Input = styled.input`
  font-size: 32px;
  border: none;
  width: 24px;
  height: 48px;
  padding: 12px;
  border-radius: 8px;
  text-align: center;
`
