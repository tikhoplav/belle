import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { ModalDemo } from './demo/ModalDemo'
import { MenuDemo } from './demo/MenuDemo'
import { ToastDemo } from './demo/ToastDemo'
import { DemoSpreadInput } from './demo/DemoSpreadInput'

function App() {
  return (
    <>
      <div>
        <a
          href='https://vitejs.dev'
          target='_blank'
        >
          <img
            src={viteLogo}
            className='logo'
            alt='Vite logo'
          />
        </a>
        <a
          href='https://react.dev'
          target='_blank'
        >
          <img
            src={reactLogo}
            className='logo react'
            alt='React logo'
          />
        </a>
      </div>
      <h1>Vite + React</h1>

      <div className='card'>
        <ModalDemo />
      </div>

      <div className='card'>
        <MenuDemo />
      </div>

      <div className='card'>
        <ToastDemo />
      </div>

      <div className='card'>
        <DemoSpreadInput />
      </div>
    </>
  )
}

export default App
