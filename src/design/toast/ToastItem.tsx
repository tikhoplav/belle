import styled from 'styled-components'
import type { Toast } from './types'
import { AnimatePresence, motion } from 'framer-motion'
import { useEffect, useState } from 'react'

export function ToastItem({
  id,
  title,
  body,
  icon,
  isClosable,
  onCloseComplete,
  duration,
}: Toast) {
  const [show, setShow] = useState<boolean>(true)

  useEffect(() => {
    if (!duration) return

    const to = setTimeout(() => setShow(false), duration)

    return () => {
      clearTimeout(to)
    }
  }, [setShow, duration])

  return (
    <AnimatePresence onExitComplete={() => onCloseComplete?.(id)}>
      {show && (
        <Root
          initial={{ x: '110%', opacity: 0.6 }}
          animate={{ x: 0, opacity: 1 }}
          exit={{ x: '110%', opacity: 0 }}
        >
          {icon && <Andorment>{icon}</Andorment>}

          <Body>
            <Title>{title}</Title>
            {body}
          </Body>

          {isClosable && (
            <Andorment>
              <Close onClick={() => setShow(false)}>x</Close>
            </Andorment>
          )}
        </Root>
      )}
    </AnimatePresence>
  )
}

const Root = styled(motion.div)`
  background: #ffffff;
  color: #000000;
  padding: 12px;
  border-radius: 12px;
  display: flex;
  gap: 12px;
`

const Body = styled.div`
  flex: 1 1 100%;
  display: flex;
  flex-flow: column nowrap;
  gap: 6px;
`

const Title = styled.div`
  font-weight: 600;
`

const Andorment = styled.div`
  flex: 0 0 32px;
  display: flex;
  align-items: flex-start;
  justify-content: center;
`

const Close = styled.div`
  font-face: monospace;
  background: #c3c3c3;
  color: #000000;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
`
