import { useCallback, useReducer } from 'react'
import { Toast } from './types'
import { ToastContext } from './ToastContext'
import { ToastHub } from './ToastHub'

interface Add {
  type: 'add'
  payload: Toast
}

interface Update {
  type: 'update'
  payload: Pick<Toast, 'id'> & Partial<Toast>
}

interface Remove {
  type: 'remove'
  payload: Toast['id']
}

type Action = Add | Update | Remove
type State = Record<Toast['id'], Toast>

function reducer(state: State, { type, payload }: Action): State {
  switch (type) {
    case 'add':
      return { ...state, [payload.id]: payload }
    case 'update':
      return { ...state, [payload.id]: { ...state[payload.id], ...payload } }
    case 'remove':
      return Object.keys(state)
        .filter(id => id !== payload)
        .reduce((acc, cur) => ({ ...acc, [cur]: state[cur] }), {})
    default:
      throw new Error('Unknown action')
  }
}

const initialState: State = {}

export function ToastProvider({ children }: { children: Children }) {
  const [state, dispatch] = useReducer(reducer, initialState)

  const add = useCallback(
    (toast: Toast) => {
      dispatch({ type: 'add', payload: toast })
    },
    [dispatch],
  )

  const update = useCallback(
    (toast: Pick<Toast, 'id'> & Partial<Toast>) => {
      dispatch({ type: 'update', payload: toast })
    },
    [dispatch],
  )

  const remove = useCallback(
    (id: Toast['id']) => {
      dispatch({ type: 'remove', payload: id })
    },
    [dispatch],
  )

  return (
    <ToastContext.Provider
      value={{
        toasts: state,
        add,
        update,
        remove,
      }}
    >
      {children}

      <ToastHub />
    </ToastContext.Provider>
  )
}
