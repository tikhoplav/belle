import { ButtonHTMLAttributes, useContext } from 'react'
import styled from 'styled-components'
import { MenuContext } from './MenuContext'

export interface MenuButtonProps
  extends Omit<ButtonHTMLAttributes<HTMLButtonElement>, 'onClick'> {
  //
}

function Base(props: MenuButtonProps) {
  const { onOpen, setAnchor } = useContext(MenuContext)

  return (
    <Button
      ref={setAnchor}
      {...props}
      onClick={onOpen}
    />
  )
}

const Button = styled.button``

export const MenuButton = styled(Base)``
