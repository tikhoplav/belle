import { MouseEventHandler, useCallback, useContext } from 'react'
import { motion } from 'framer-motion'
import styled from 'styled-components'
import { ModalContext } from './ModalContext'

export const ModalOverlay = styled(Base)``

function Base({ className }: { className?: string }) {
  const { canEscape, onClose } = useContext(ModalContext)

  const onClick = useCallback<MouseEventHandler>(
    e => {
      e.stopPropagation()
      if (!canEscape) return
      onClose?.()
    },
    [canEscape, onClose],
  )

  return (
    <Root
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 0.32 }}
      className={className}
      onClick={onClick}
    />
  )
}

const Root = styled(motion.div)`
  position: fixed;
  left: 0;
  right: 0;
  top: 0;
  width: 100vw;
  height: 100vh;
  background: #00000064;
  backdrop-filter: blur(2px);
`
