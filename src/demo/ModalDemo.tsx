import { Modal, ModalContent, ModalOverlay } from '@design/modal'
import { useState } from 'react'

export function ModalDemo() {
  const [isOpen, setOpen] = useState<boolean>(false)

  return (
    <>
      <button onClick={() => setOpen(true)}>Open modal</button>

      <Modal
        isOpen={isOpen}
        onClose={() => setOpen(false)}
        canEscape
      >
        <ModalOverlay />
        <ModalContent>
          <div>Hello, World!!</div>

          <button onClick={() => setOpen(false)}>Close</button>
        </ModalContent>
      </Modal>
    </>
  )
}
