import { useEffect, useRef, useState } from 'react'
import styled, { keyframes } from 'styled-components'

export function Flusher({
  children,
  delay,
}: {
  children: Children
  delay: number
}) {
  const prev = useRef<Children>(children)
  const [cur, setCur] = useState<Children>(children)
  const [isUnloading, setUnloading] = useState<boolean>(false)

  useEffect(() => {
    if (prev.current == children) return

    setUnloading(true)
    const to = setTimeout(() => {
      setCur(children)
      setUnloading(false)
    }, delay)

    return () => {
      clearTimeout(to)
      setUnloading(false)
    }
  }, [children, prev, delay, setCur, setUnloading])

  return (
    <Root
      $delay={delay}
      $isUnloading={isUnloading}
    >
      {cur}
    </Root>
  )
}

const fadeIn = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`

const fadeOut = keyframes`
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
`

const Root = styled.div<{
  $delay: number
  $isUnloading: boolean
}>`
  width: fit-content;
  height: fit-content;

  animation-name: ${prop => (prop.$isUnloading ? fadeOut : fadeIn)};
  animation-duration: ${prop => prop.$delay}ms;
  animation-timing-function: ease-out;
  animation-fill-mode: both;
`
