import { createContext } from 'react'

export interface ModalCtx {
  /**
   *  Shows if modal is currently open, can only be changed from within the
   *  modal by calling `onClose`.
   */
  isOpen: boolean

  /**
   *  Callback that is invoked when user interacts in specific ways (ex
   *  hit of an `Esc` key, or click on a overlay / close icon).
   */
  onClose?: () => void

  /**
   *  Shows if modal can be escaped by pressing the `Esc` key, or clicking
   *  on the overlay (if present). Is `false` by default, meaning that user
   *  has to perform some specific actions in order to close the modal.
   */
  canEscape?: boolean

  /**
   *  Triggered as soon as all the elements are animated on exit.
   */
  onExitComplete?: () => void
}

export const ModalContext = createContext<ModalCtx>({
  isOpen: false,
})

ModalContext.displayName = 'Modal Context'
