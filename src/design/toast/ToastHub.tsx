import styled from 'styled-components'
import { Portal } from '../Portal'
import { ToastItem } from './ToastItem'
import { useContext } from 'react'
import { ToastContext } from './ToastContext'

export function ToastHub() {
  const { toasts, remove } = useContext(ToastContext)

  return (
    <Portal>
      <Container>
        {Object.values(toasts).map(toast => (
          <ToastItem
            key={toast.id}
            {...toast}
            onCloseComplete={id => {
              remove(id)
              toast.onCloseComplete?.(id)
            }}
          />
        ))}
      </Container>
    </Portal>
  )
}

const Container = styled.div`
  position: absolute;
  width: 380px;
  height: 100vh;
  right: 0;
  bottom: 0;
  padding: 12px 12px 12px 60px;
  overflow: hidden;

  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: flex-end;
  gap: 12px;
`
