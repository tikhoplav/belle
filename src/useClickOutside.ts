import { useEffect } from 'react'

export function useClickOutside(cb?: () => void, ref?: HTMLElement | null) {
  useEffect(() => {
    if (!ref) return
    const mousedown = (e: MouseEvent) => {
      if (ref.contains(e.target as Node)) return
      cb?.()
    }
    document.addEventListener('mousedown', mousedown)
    return () => {
      document.removeEventListener('mousedown', mousedown)
    }
  }, [ref, cb])
}
