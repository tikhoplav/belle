import { createContext } from 'react'
import { Toast } from './types'

export interface ToastCtx {
  toasts: Record<Toast['id'], Toast>
  add: (toast: Toast) => void
  update: (toast: Pick<Toast, 'id'> & Partial<Toast>) => void
  remove: (id: Toast['id']) => void
}

export const ToastContext = createContext<ToastCtx>({
  toasts: {},
  add: () => {},
  update: () => {},
  remove: () => {},
})

ToastContext.displayName = 'ToastContext'
