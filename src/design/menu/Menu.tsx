import { useCallback, useState } from 'react'
import { MenuCtx, MenuContext } from './MenuContext'

export interface MenuProps
  extends Omit<MenuCtx, 'onOpen' | 'anchor' | 'setAnchor'> {
  children?: Children
}

export function Menu({ children, isOpen, ...rest }: MenuProps) {
  const [_open, _setOpen] = useState<boolean>(isOpen || false)
  const [anchor, setAnchor] = useState<HTMLButtonElement | null>(null)

  const onOpen = useCallback(() => _setOpen(true), [_setOpen])
  const onClose = useCallback(() => _setOpen(false), [_setOpen])

  return (
    <MenuContext.Provider
      value={{
        isOpen: _open,
        onOpen,
        onClose,
        anchor,
        setAnchor,
        ...rest,
      }}
    >
      {children}
    </MenuContext.Provider>
  )
}
