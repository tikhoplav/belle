import { ReactElement } from 'react'

declare global {
  type Child = boolean | string | ReactElement | JSX.Element
  type Children =
    | string
    | ReactElement
    | ReactElement[]
    | JSX.Element
    | JSX.Element[]

  type Address = `0x${string}`
}

export {}
