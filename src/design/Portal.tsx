import { RefObject, useLayoutEffect, useRef, useState } from 'react'
import { createPortal } from 'react-dom'

export interface PortalProps {
  parentRef?: RefObject<HTMLElement | null>
  children?: Children
  className?: string
}

export function Portal({ parentRef, children, className }: PortalProps) {
  const [temp, setTemp] = useState<HTMLElement | null>(null)
  const portal = useRef<HTMLElement | null>(null)

  const [, forceUpdate] = useState({})

  useLayoutEffect(() => {
    if (!temp) return

    const doc = temp.ownerDocument
    const host = parentRef?.current ?? doc.body

    if (!host) return

    portal.current = doc.createElement('div')
    if (className) portal.current.className = className
    host.appendChild(portal.current)

    forceUpdate({})

    return () => {
      if (portal.current && host.contains(portal.current)) {
        host.removeChild(portal.current)
      }
    }
  }, [temp, parentRef, className])

  return portal.current ? (
    createPortal(children, portal.current)
  ) : (
    <span ref={el => !!el && setTemp(el)} />
  )
}
