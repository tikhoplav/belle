export interface Toast {
  /**
   *  Unique ID of the toast. Used to prevent duplicates.
   */
  id: string

  /**
   *  Title of the toast;
   */
  title?: Child

  /**
   *  Body of the toast;
   */
  body?: Child

  /**
   *  Icon of the toast;
   */
  icon?: Child

  /**
   *  If `true` the toast can be dismissed.
   */
  isClosable?: boolean

  /**
   *  A callback executed right after the toast is dismissed.
   */
  onCloseComplete?: (id: string) => void

  /**
   *  A delay in `ms` before the toast is dismissed automatically.
   *  If not set or `0`, the toast will not be dismissed automatically.
   */
  duration?: number
}
