import { Menu, MenuButton, MenuList } from '@design/menu'

export function MenuDemo() {
  return (
    <Menu>
      <MenuButton>Menu</MenuButton>

      <MenuList>Hello, menu!!</MenuList>
    </Menu>
  )
}
