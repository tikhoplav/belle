import { motion, usePresence } from 'framer-motion'
import { useEffect } from 'react'
import ReactFocusLock from 'react-focus-lock'
import { RemoveScroll } from 'react-remove-scroll'
import styled from 'styled-components'

export function ModalContent({
  children,
  className,
}: {
  children?: Children
  className?: string
}) {
  const [isPresent, safeToRemove] = usePresence()

  useEffect(() => {
    if (!isPresent && safeToRemove) {
      setTimeout(safeToRemove)
    }
  }, [isPresent, safeToRemove])

  return (
    <ReactFocusLock
      returnFocus
      className={className}
    >
      <RemoveScroll forwardProps>
        <Wrapper>
          <Container
            initial={{ opacity: 0, scale: 0.8 }}
            animate={{ opacity: 1, scale: 1 }}
            exit={{ opacity: 0 }}
            transition={{ duration: 0.32 }}
          >
            {children}
          </Container>
        </Wrapper>
      </RemoveScroll>
    </ReactFocusLock>
  )
}

const Wrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  width: 100vw;
  height: 100vh;

  display: flex;
  align-items: center;
  justify-content: center;

  pointer-events: none;
`

const Container = styled(motion.div)`
  pointer-events: auto;
`
