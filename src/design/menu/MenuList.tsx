import { useContext, useState } from 'react'
import { AnimatePresence, motion } from 'framer-motion'
import styled from 'styled-components'
import { usePopper } from 'react-popper'
import { MenuContext } from './MenuContext'
import { useClickOutside } from '../../useClickOutside'

export function MenuList({
  children,
  className,
}: {
  children?: Children
  className?: string
}) {
  const { isOpen, onClose, anchor } = useContext(MenuContext)
  const [popper, setPopper] = useState<HTMLElement | null>(null)

  useClickOutside(onClose, popper)

  const { styles, attributes } = usePopper(anchor, popper, {
    placement: 'bottom',
  })

  return (
    <AnimatePresence>
      {isOpen && (
        <Container
          ref={setPopper}
          className={className}
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ duration: 0.32 }}
          style={styles.popper}
          {...attributes.popper}
        >
          {children}
        </Container>
      )}
    </AnimatePresence>
  )
}

const Container = styled(motion.div)`
  padding: 0.8rem;
  background: #ffffff;
  border-radius: 12px;

  color: #2c2c2c;
`
