import { useToast } from '@design/toast'

/**
 *  Requires the App to be wrapped with `ToastProvider`.
 */
export function ToastDemo() {
  const toast = useToast()

  return (
    <button
      onClick={() =>
        toast({
          id: (200 * (Math.random() + 1)).toString(16),
          title: 'Demo toast',
          body: 'This is the demo notification',
          icon: '@',
          isClosable: true,
          duration: 3200,
        })
      }
    >
      Notify
    </button>
  )
}
