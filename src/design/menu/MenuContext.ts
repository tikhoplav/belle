import { createContext } from 'react'

export interface MenuCtx {
  /**
   *  Determines if menu is open by default.
   */
  isOpen?: boolean

  onOpen?: () => void
  onClose?: () => void

  /**
   * An element to position the menu against.
   */
  anchor: HTMLButtonElement | null

  setAnchor: (el: HTMLButtonElement) => void
}

export const MenuContext = createContext<MenuCtx>({
  anchor: null,
  setAnchor: () => {},
})

MenuContext.displayName = 'MenuContext'
